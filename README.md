My solutions for [P-99: Ninety-Nine Prolog Problems](https://sites.google.com/site/prologsite/prolog-problems)

Each category is organised into directories. One file for each problem.
