% 1.17 (*) Split a list into two parts; the length of the first part is given.
% Do not use any predefined predicates.

% split(List, Int, List1, List2)
% Split the list into List1 and List2
% The integer is the size of List1

% Given the length of the first list zero,
%     the first list is the empty list,
%     and the second list is the original list
split(L, 0, [], L).
% Splitting the list without the head with one less N would result in
%     the tail of L1 and the same L2
split([X|Xs], N, [X|TL1], L2) :-
    M is N-1,
    split(Xs, M, TL1, L2).
