% 1.11 (*) Modified run-length encoding.
% Modify the result of problem 1.10 in such a way that if an element has no duplicates it is simply copied into the result list. Only elements with duplicates are transferred as [N,E] terms.

:- ensure_loaded("09_pack").

% Same as in 1.10, just add the check for length in runlength/2

runlength_modified([], []).
runlength_modified([ [X|Xs] | Rest], [ [L,X] | NextRest]) :-
    length([X|Xs], L),
    L > 1,
    runlength_modified(Rest, NextRest).
runlength_modified([ [X|Xs] | Rest], [ X | NextRest]) :-
    length([X|Xs], L),
    L == 1,
    runlength_modified(Rest, NextRest).


encode_modified([], []).
encode_modified(Xs, Ys) :-
    pack(Xs, Ps),
    runlength_modified(Ps, Ys).
