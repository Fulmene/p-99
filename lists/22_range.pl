% 1.22 (*) Create a list containing all integers within a given range.

% range(First, Last, List)

% The range from N to N consists of just N
range(N, N, [N]).
% The range from N to M consists of N and the range from N+1 to M
range(N, M, [N|Xs]) :-
    K is N+1,
    range(K, M, Xs).
