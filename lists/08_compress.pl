% 1.08 (**) Eliminate consecutive duplicates of list elements.
% If a list contains repeated elements they should be replaced with a single copy of the element. The order of the elements should not be changed.

% Compressing the empty list gives the empty list
compress([], []).
% Compressing a list with two duplicate elements at the head
%     is the same as compressing with only one of the duplicate
compress([H,H|T], X) :-
    compress([H|T], X).
% Otherwise, compressing the list keeps the head and compress its tail
compress([H|T], [H|X]) :-
    compress(T, X).
