% 1.03 (*) Find the K'th element of a list.
% The first element in the list is number 1.

% element_at(List, Int, Element)

% Element 1 of any list is the head
element_at([X|_], 1, X).
% Element N of any list is element N-1 of its tail
element_at([_|T], N, X) :-
    M is N-1,
    element_at(T, M, X).
