% 1.21 (*) Insert an element at a given position into a list.

% insert_at(Element, List, Position, NewList)

% Inserting at position 1 is at the head
insert_at(E, Xs, 1, [E|Xs]).
% Inserting at position N is the same as inserting at position N-1 of the tail
insert_at(E, [X|Xs], N, [X|Rs]) :-
    M is N-1,
    insert_at(E, Xs, M, Rs).
