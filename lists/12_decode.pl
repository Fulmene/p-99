% 1.12 (**) Decode a run-length encoded list.
% Given a run-length code list generated as specified in problem 1.11. Construct its uncompressed version.

% decode(EncodedList, List)

% Decoding the empty list gives the empty list
decode([], []).
% Decoding [1, X] acts as the base case for [L, X]
decode([[1,X] | Rest], [X|NRest]) :-
    decode(Rest, NRest).
% Decoding [L, X] is decreasing L for one more X until L hits one
decode([[L,X] | Rest], [X|NRest]) :-
    L > 1,
    NL is L-1,
    decode([[NL,X] | Rest], NRest).
% The case for non-list, just a single element
decode([X|Rest], [X|NRest]) :-
    decode(Rest, NRest).
