% 1.25 (*) Generate a random permutation of the elements of a list.

ensure_loaded("23_rnd_select.pl").

% rnd_permu(List, NewList)

rnd_permu(L, NL) :-
    length(L, Len),
    rnd_select(L, Len, NL).
