% 1.24 (*) Lotto: Draw N different random numbers from the set 1..M.
% The selected numbers shall be put into a result list.

:- ensure_loaded("22_range.pl").
:- ensure_loaded("23_rnd_select.pl").

% lotto(Number, Max, List)

lotto(N, Max, L) :-
    range(1, Max, R),
    rnd_select(R, N, L).
