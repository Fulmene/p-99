% 1.23 (**) Extract a given number of randomly selected elements from a list.
%The selected items shall be put into a result list.

:- ensure_loaded("20_remove_at.pl").

% rnd_select(List, Int, Result)

% Selecting zero elements randomly gives the empty list
rnd_select(_, 0, []).
% Select 1 element and N-1 elements from the remaining list
rnd_select(Xs, N, [X|Res]) :-
    length(Xs, L),
    random_between(1, L, P),
    remove_at(X, Xs, P, Rs),
    M is N-1,
    rnd_select(Rs, M, Res).
