% 1.13 (**) Run-length encoding of a list (direct solution).
% Implement the so-called run-length encoding data compression method directly. I.e. don't explicitly create the sublists containing the duplicates, as in problem 1.09, but only count them. As in problem 1.11, simplify the result list by replacing the singleton terms [1,X] by X.

% encode_direct(List, EncodedList)

% Encoding the empty list gives the empty list
encode_direct([], []).
% Add more length for more duplicates
encode_direct([H|T], [[L,H]|NT]) :-
    encode_direct(T, [[PL,H]|NT]),
    L is PL+1.
% Transition from 1 element (X) to [2, X]
encode_direct([H|T], [[2,H]|NT]) :-
    encode_direct(T, [H|NT]).
% The case for the first element of the duplicates
encode_direct([H|T], [H|NT]) :-
    encode_direct(T, NT).
