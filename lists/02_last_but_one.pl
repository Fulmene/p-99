% 1.02 (*) find the last but one element of a list.
% (de: zweitletztes Element, fr: avant-dernier élément)

% my_last_but_one(List, LastButOneElement)
% Like in 1.01

% The last but one element of a list of two elements is the head.
my_last_but_one([X, _], X).
% The last but one element of any list is the last but one of its tail.
my_last_but_one([_|T], X) :- my_last_but_one(T, X).
