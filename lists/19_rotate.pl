% 1.19 (**) Rotate a list N places to the left.

:- ensure_loaded("17_split.pl").

% rotate(List, Int, RotatedList)

% Rotating by the length of the list is the same as not rotating at all,
%     thus use modulo on the number
% Split the list at the number position then swap the halves
rotate(L, N, X) :-
    length(L, Len),
    M is N mod Len,
    split(L, M, L1, L2),
    append(L2, L1, X).
