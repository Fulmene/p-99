% 1.09 (**) Pack consecutive duplicates of list elements into sublists.
% If a list contains repeated elements they should be placed in separate sublists.

% pack(List, PackedList)

% The packed empty list is the empty list
pack([], []).
% Packing a list with two duplicate elements at the head,
%     the head is added into the sublist
pack([H,H|T], [[H|Hs]|N]) :-
    pack([H|T], [Hs|N]).
% Otherwise, the head forms a new sublist
pack([H|T], [[H]|N]) :-
    pack(T, N).
