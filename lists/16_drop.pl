% 1.16 (**) Drop every N'th element from a list.

% drop(List, Position, Current, NewList)

% Dropping from empty list gives empty list
drop([], _, _, []).
% If the count is N, drop the element and reset the count to 1
drop([_|T], N, N, X) :-
    drop(T, N, 1, X).
% Otherwise, add count
drop([H|T], N, M, [H|X]) :-
    M < N,
    NextM is M+1,
    drop(T, N, NextM, X).


% drop(List, Int, NewList)

% Dropping from empty list gives empty list
drop([], _, []).
% Use drop/4 to add element counting
drop([H|T], N, X) :-
    drop([H|T], N, 1, X).
