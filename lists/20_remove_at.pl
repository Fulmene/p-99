% 1.20 (*) Remove the K'th element from a list.

% remove_at(RemovedElement, List, Position, NewList)

% Removing the first element is the head
remove_at(X, [X|Xs], 1, Xs).
% Removing element N is the same as removing element N-1 of the tail
remove_at(E, [X|Xs], N, [X|Rs]) :-
    M is N-1,
    remove_at(E, Xs, M, Rs).
