% 1.10 (*) Run-length encoding of a list.
% Use the result of problem 1.09 to implement the so-called run-length encoding data compression method. Consecutive duplicates of elements are encoded as terms [N,E] where N is the number of duplicates of the element E.

:- ensure_loaded("09_pack.pl").

% runlength
% Gives a run-length encoded list of a packed list

% Run-length encoding the empty list gives the empty list
runlength([], []).
% Run-length encoding of any packed list is simply counting elements in the head list,
%     and use it as the head before the run-length encoding of the rest.
runlength([ [X|Xs] | Rest], [[L,X] | NRest]) :-
    length([X|Xs], L),
    runlength(Rest, NRest).


% encode(List, EncodedList)
encode([], []).
encode(Xs, Ys) :-
    pack(Xs, Ps),
    runlength(Ps, Ys).
