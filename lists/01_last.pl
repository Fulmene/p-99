% 1.01 (*) Find the last element of a list.

% my_last(List, LastElement).

% The last element of a singleton list is the singleton element itself.
my_last([X], X).
% The last element of any list is the last element of its tail.
my_last([_|T], X) :- my_last(T, X).
