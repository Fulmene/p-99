% 1.06 (*) Find out whether a list is a palindrome.
% A palindrome can be read forward or backward; e.g. [x,a,m,a,x].

remove_last([_], []).
remove_last([H|T], [H|X]) :- remove_last(T, X).

% palindrome(List)

% The empty list is a palindrome
palindrome([]).
% The singleton list is a palindrome
palindrome([_]).
% Any list is a palindrome if the head and the last element is the same,
%     and the remaining list is also a palindrome
palindrome([H|T]) :-
    last(T, L), H == L,
    remove_last(T, N), palindrome(N).
