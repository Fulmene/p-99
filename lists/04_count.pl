% 1.04 (*) Find the number of elements of a list.

% my_count(List, Int)

% The number of elements of the empty list is zero
my_count([], 0).
% The number of elements of any list is one plus the number of elements of its tail
my_count([_|T], X) :- my_count(T, Y), X is Y+1.
