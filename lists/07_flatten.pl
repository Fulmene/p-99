% 1.07 (**) Flatten a nested list structure.
% Transform a list, possibly holding lists as elements into a 'flat' list by replacing each list with its elements (recursively).

% my_flatten(List, FlattenedList)

% Flattening the empty list gives the empty list
my_flatten([], []).
% For any list, its flatten is flattening both its head and its tail
my_flatten([H|T], X) :-
    is_list(H),
    my_flatten(H, NewH),
    my_flatten(T, NewT),
    append(NewH, NewT, X).
% If the head isn't a list, then the flatten of its head is itself
my_flatten([H|T], [H|Y]) :-
    my_flatten(T, Y).
