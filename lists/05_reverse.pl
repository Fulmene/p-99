% 1.05 (*) Reverse a list.

% my_reverse(List, ReversedList)
% The reverse of the empty list is the empty list
my_reverse([], []).
% The reverse of any list is the reverse of its tail appended by its head.
my_reverse([H|T], X) :-
    my_reverse(T, RevT),
    append(RevT, [H], X).
