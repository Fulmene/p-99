% 1.15 (**) Duplicate the elements of a list a given number of times.

% replicate(Element, Int, List)
% Create a list of replicated elements by the given number

% Replicating by zero gives the empty list
replicate(_, 0, []).
% Replicating by N adds the element to the head and decrease the number by one
replicate(X, N, [X|Rest]) :-
    LN is N-1,
    replicate(X, LN, Rest).


% dupli(List, Int, DuplicatedList)

% Duplicating the empty list gives the empty list
dupli([], _, []).
% Duplicate by replicating the head and duplicate the tail
dupli([H|T], N, X) :-
    replicate(H, N, RH),
    dupli(T, N, NT),
    append(RH, NT, X).
