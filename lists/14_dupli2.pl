% 1.14 (*) Duplicate the elements of a list.

% dupli(List, DuplicatedList)

% Duplicating the empty list gives the empty list
dupli([], []).
% Duplicate the head and the rest of the list
dupli([H|T], [H,H|NT]) :- dupli(T, NT).
