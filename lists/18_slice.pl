% 1.18 (**) Extract a slice from a list.
% Given two indices, I and K, the slice is the list containing the elements between the I'th and K'th element of the original list (both limits included). Start counting the elements with 1.

% If the counter exceeds K, then the result is empty list
slice(_, _, K, N, []) :- N > K.
% If the counter is less than I, skip the result
slice([_|Xs], I, K, N, Slice) :-
    N < I,
    M is N+1,
    slice(Xs, I, K, M, Slice).
% Otherwise, take the slice
slice([X|Xs], I, K, N, [X|Slice]) :-
    M is N+1,
    slice(Xs, I, K, M, Slice).


% slice(List, I, K, Slice)

% Use slice/5 to add counter
slice(Xs, I, K, Slice) :- 
    slice(Xs, I, K, 1, Slice).
